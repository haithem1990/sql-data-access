package com.sql.data.access;

import com.sql.data.access.datasource.DataSource;
import com.sql.data.access.datasource.MySqlDataSourceConnection;
import com.sql.data.access.model.CustomerType;
import com.sql.data.access.repositories.CustomerTypeRepository;
import com.sql.data.access.repositories.CustomerTypeRepositoryImpl;
import com.sql.data.access.template.JdbcTemplate;

import java.util.List;

public class DataAccessWithRepositoryAndTemplateDesignPattern {

    public static void main(String[] args) {
        try {
            // Initialize data source impl
            DataSource dataSource = new MySqlDataSourceConnection();
            JdbcTemplate<CustomerType> customerTypeJdbcTemplate = new JdbcTemplate<>(dataSource);
            // initialize customer type access provider
            CustomerTypeRepository customerTypeRepository = new CustomerTypeRepositoryImpl(customerTypeJdbcTemplate);
            // findAll
            print(customerTypeRepository.findAll());
            // save
            Integer rtDefaultId = customerTypeRepository.save(new CustomerType("R_T_DEFAULT"));
            Integer rtCorporateId = customerTypeRepository.save(new CustomerType("R_T_CORPORATE"));
            // update
            customerTypeRepository.update(new CustomerType(rtCorporateId, "R_T_VIP"));
            // findAll
            print(customerTypeRepository.findAll());
            // delete All
            customerTypeRepository.deleteAll();
            // findAll
            print(customerTypeRepository.findAll());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void print(List<CustomerType> customerTypeList) {
        if (customerTypeList == null || customerTypeList.isEmpty()) {
            System.out.println("empty");
        } else {
            customerTypeList.stream().forEach(customerType -> System.out.printf("%s | %s \n ", customerType.getCustomerTypeId(), customerType.getCustomerTypeName()));
        }
        System.out.println("############################");
    }
//
//    private static void firstNote() {
//        System.out.println("#################### START FIRST NOTE #################");
//        System.out.println("with basic access module we will discover how we can implement a simple crud with java and sql database ( mysql in this case )");
//        System.out.println("with basic access module we will discover how managing sql and business logic code will make it scattered and tangled");
//        System.out.println("#################### END FIRST NOTE #################");
//    }
//
//    private static void finalNote() {
//        System.out.println("#################### START FINAL NOTE #################");
//        System.out.println("In CustomerTypeAccessProvider class we have managing sql and business logic code");
//        System.out.println("In CustomerTypeAccessProvider class we have a duplication of code ");
//        System.out.println("#################### END FINAL NOTE #################");
//    }
}
