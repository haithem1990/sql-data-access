package com.sql.data.access.datasource;

import java.sql.Connection;

public interface DataSource {

    Connection getConnection() throws Exception;
}
