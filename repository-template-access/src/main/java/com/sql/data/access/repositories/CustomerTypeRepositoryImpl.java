package com.sql.data.access.repositories;

import com.sql.data.access.model.CustomerType;
import com.sql.data.access.template.JdbcTemplate;

import java.util.List;

public class CustomerTypeRepositoryImpl implements CustomerTypeRepository {

    private JdbcTemplate<CustomerType> jdbcTemplate;

    public CustomerTypeRepositoryImpl(JdbcTemplate<CustomerType> jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public List<CustomerType> findAll() throws Exception {
        return jdbcTemplate
                .findAll(
                        "select * from customer_type",
                        (resultSet) -> {
                            return new CustomerType(resultSet.getInt("customer_type_id"),
                                    resultSet.getString("customer_type_name")
                            );
                        });
    }

    public Integer save(CustomerType customerType) throws Exception {
        return jdbcTemplate.save("insert into customer_type (customer_type_name) values(?)", new Object[]{customerType.getCustomerTypeName()}, "customer_type_id");
    }

    public void update(CustomerType customerType) throws Exception {
        jdbcTemplate.update("update customer_type set customer_type_name= ? where customer_type_id = ?", new Object[]{customerType.getCustomerTypeName(), customerType.getCustomerTypeId()});
    }

    public void deleteAll() throws Exception {
        jdbcTemplate.deleteAll("delete from customer_type");
    }

}
