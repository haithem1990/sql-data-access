package com.sql.data.access.repositories;

import com.sql.data.access.model.CustomerType;
import com.sql.data.access.repository.CrudRepository;

public interface CustomerTypeRepository extends CrudRepository<CustomerType> {
}
