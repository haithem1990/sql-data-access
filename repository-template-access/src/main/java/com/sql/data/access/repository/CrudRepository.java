package com.sql.data.access.repository;

import java.util.List;

public interface CrudRepository<T> {

    List<T> findAll() throws Exception;

    Integer save(T t) throws Exception;

    void update(T t) throws Exception;

    void deleteAll() throws Exception;
}
