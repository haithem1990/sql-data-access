package com.sql.data.access.template;

import com.sql.data.access.datasource.DataSource;
import com.sql.data.access.mapper.RowMapper;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JdbcTemplate<T> {

    private DataSource dataSource;

    public JdbcTemplate(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<T> findAll(String sqlQuery, RowMapper<T> rowMapper) throws Exception {
        // initialize
        List<T> list = new ArrayList<>();
        // get connection
        Connection connection = dataSource.getConnection();
        // prepare statement
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        // execute query
        ResultSet resultSet = preparedStatement.executeQuery();
        // mapping
        while (resultSet.next()) {
            list.add(rowMapper.mapRow(resultSet));
        }
        connection.close();
        return list;
    }

    public Integer save(String sqlQuery, Object[] params, String idFieldName) throws Exception {
        // get connection
        Connection connection = dataSource.getConnection();
        // prepare statement
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, new String[]{idFieldName});
        int i = 1;
        for (Object param : params) {
            preparedStatement.setObject(i, param);
            i++;
        }
        // execute query
        preparedStatement.executeUpdate();
        ResultSet resultSet = preparedStatement.getGeneratedKeys();
        Integer id;
        if (resultSet.next()) {
            id = resultSet.getInt(1);
        } else {
            throw new SQLException();
        }
        connection.close();
        return id;
    }

    public void update(String sqlQuery, Object[] params) throws Exception {
        // get connection
        Connection connection = dataSource.getConnection();
        // prepare statement
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        int i = 1;
        for (Object param : params) {
            preparedStatement.setObject(i, param);
            i++;
        }
        // execute query
        preparedStatement.executeUpdate();
    }

    public void deleteAll(String sqlQuery) throws Exception {
        // get connection
        Connection connection = dataSource.getConnection();
        // prepare statement
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        // execute query
        preparedStatement.executeUpdate();
        connection.close();
    }
}
