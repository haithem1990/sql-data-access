package com.sql.data.access.basic.exception;

public class BasicSqlException extends RuntimeException {

    public BasicSqlException(Throwable throwable) {
        super(throwable);
    }

    public BasicSqlException(String msg) {
        super(msg);
    }
}
