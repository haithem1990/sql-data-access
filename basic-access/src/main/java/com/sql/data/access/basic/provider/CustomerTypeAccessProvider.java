package com.sql.data.access.basic.provider;

import com.sql.data.access.basic.datasource.DataSource;
import com.sql.data.access.basic.exception.BasicSqlException;
import com.sql.data.access.basic.model.CustomerType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerTypeAccessProvider implements ICustomerTypeAccessProvider {

    public static final String CUSTOMER_TYPE_ID = "customer_type_id";
    public static final String CUSTOMER_TYPE_NAME = "customer_type_name";
    private DataSource dataSource;

    public CustomerTypeAccessProvider(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    public List<CustomerType> findAll() {
        Connection connection = null;
        try {
            // initialize
            List<CustomerType> customerTypeList = new ArrayList<>();
            // get connection
            connection = dataSource.getConnection();
            // prepare statement
            PreparedStatement preparedStatement = connection.prepareStatement("select * from customer_type");
            // execute query
            ResultSet resultSet = preparedStatement.executeQuery();
            // mapping
            while (resultSet.next()) {
                CustomerType customerType = new CustomerType(resultSet.getInt(CUSTOMER_TYPE_ID), resultSet.getString(CUSTOMER_TYPE_NAME));
                customerTypeList.add(customerType);
            }
            return customerTypeList;
        } catch (Exception e) {
            throw new BasicSqlException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Integer save(CustomerType customerType) {
        Connection connection = null;
        try {
            // get connection
            connection = dataSource.getConnection();
            // prepare statement
            String[] gen = {"customer_type_id"};
            PreparedStatement preparedStatement = connection.prepareStatement("insert into customer_type (customer_type_name) values(?)", gen);
            // execute query
            preparedStatement.setString(1, customerType.getCustomerTypeName());
            preparedStatement.executeUpdate();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                throw new BasicSqlException("save customer_type failed");
            }
        } catch (Exception e) {
            throw new BasicSqlException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Integer update(CustomerType customerType) {
        Connection connection = null;
        try {
            // get connection
            connection = dataSource.getConnection();
            // prepare statement
            PreparedStatement preparedStatement = connection.prepareStatement("update customer_type set customer_type_name= ? where customer_type_id = ?");
            // execute query
            preparedStatement.setString(1, customerType.getCustomerTypeName());
            preparedStatement.setInt(2, customerType.getCustomerTypeId());
            return preparedStatement.executeUpdate();

        } catch (Exception e) {
            throw new BasicSqlException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void deleteAll() {
        Connection connection = null;
        try {
            // initialize
            List<CustomerType> customerTypeList = new ArrayList<>();
            // get connection
            connection = dataSource.getConnection();
            // prepare statement
            PreparedStatement preparedStatement = connection.prepareStatement("delete from customer_type");
            // execute query
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            throw new BasicSqlException(e);
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
