package com.sql.data.access.basic.datasource;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySqlDataSourceConnection implements DataSource {

    public static final String USER_NAME = "root";
    public static final String PASSWORD = "root";
    public static final String URL = "jdbc:mysql://localhost:3306/sql_db_access";

    public Connection getConnection() throws Exception {
        return DriverManager.getConnection(URL, USER_NAME, PASSWORD);
    }
}
