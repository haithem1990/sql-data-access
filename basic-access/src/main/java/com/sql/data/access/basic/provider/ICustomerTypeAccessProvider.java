package com.sql.data.access.basic.provider;

import com.sql.data.access.basic.model.CustomerType;

import java.util.List;

public interface ICustomerTypeAccessProvider {

    List<CustomerType> findAll();

    Integer save(CustomerType customerType);

    Integer update(CustomerType customerType);

    void deleteAll();
}
