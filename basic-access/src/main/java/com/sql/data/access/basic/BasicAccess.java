package com.sql.data.access.basic;

import com.sql.data.access.basic.datasource.DataSource;
import com.sql.data.access.basic.datasource.MySqlDataSourceConnection;
import com.sql.data.access.basic.model.CustomerType;
import com.sql.data.access.basic.provider.CustomerTypeAccessProvider;
import com.sql.data.access.basic.provider.ICustomerTypeAccessProvider;

import java.util.List;

public class BasicAccess {

    public static void main(String[] args) {

        firstNote();
        // Initialize data source impl
        DataSource dataSource = new MySqlDataSourceConnection();
        // initialize customer type access provider
        ICustomerTypeAccessProvider iCustomerTypeAccessProvider = new CustomerTypeAccessProvider(dataSource);
        // findAll
        print(iCustomerTypeAccessProvider.findAll());
        // save
        Integer defaultCustomerTypeId = iCustomerTypeAccessProvider.save(new CustomerType("DEFAULT"));
        Integer corporateCustomerTypeId = iCustomerTypeAccessProvider.save(new CustomerType("CORPORATE"));
        // findAll
        print(iCustomerTypeAccessProvider.findAll());
        // update
        iCustomerTypeAccessProvider.update(new CustomerType(defaultCustomerTypeId, "VIP"));
        // findAll
        print(iCustomerTypeAccessProvider.findAll());
        // deleteAll
        iCustomerTypeAccessProvider.deleteAll();
        // findAll
        print(iCustomerTypeAccessProvider.findAll());
        finalNote();
    }

    private static void print(List<CustomerType> customerTypeList) {
        if (customerTypeList == null || customerTypeList.isEmpty()) {
            System.out.println("empty");
        } else {
            customerTypeList.stream().forEach(customerType -> System.out.printf("%s | %s \n ", customerType.getCustomerTypeId(), customerType.getCustomerTypeName()));
        }
        System.out.println("############################");
    }

    private static void firstNote() {
        System.out.println("#################### START FIRST NOTE #################");
        System.out.println("with basic access module we will discover how we can implement a simple crud with java and sql database ( mysql in this case )");
        System.out.println("with basic access module we will discover how managing sql and business logic code will make it scattered and tangled");
        System.out.println("#################### END FIRST NOTE #################");
    }

    private static void finalNote() {
        System.out.println("#################### START FINAL NOTE #################");
        System.out.println("In CustomerTypeAccessProvider class we have managing sql and business logic code");
        System.out.println("In CustomerTypeAccessProvider class we have a duplication of code ");
        System.out.println("#################### END FINAL NOTE #################");
    }
}
